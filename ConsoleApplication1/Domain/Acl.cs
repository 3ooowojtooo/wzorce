using System;

namespace ConsoleApplication1.Domain
{
    public class Acl
    {
        public virtual int Id { get; set; }
        public virtual string Table_Name { get; set; }
        public virtual int Priority { get; set; }
        public virtual string Role_Name { get; set; }
        public virtual string Allowed_Ids { get; set; }
    }
}