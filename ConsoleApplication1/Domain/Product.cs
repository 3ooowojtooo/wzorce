using ConsoleApplication1.Security.Secure;

namespace ConsoleApplication1.Domain
{
    [Secure]
    public class Product
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Category { get; set; }
        public virtual bool Discontinued { get; set; }

        public override string ToString()
        {
            return "Id: " + Id + " Name: " + Name + " Category: " + Category + " Discontinued: " +
                   Discontinued;
        }
    }
}