namespace ConsoleApplication1.Domain

{
    public class Role
    {
        public virtual int role_id { get; set; }
        public virtual int parent_id { get; set; }
        public virtual string role_name { get; set; }
    }
}