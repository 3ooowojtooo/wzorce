using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ConsoleApplication1.Security.Secure
{
    public class SecuredEntitiesFinder
    {
        private static SecuredEntitiesFinder _securedEntitiesFinder = null;

        private List<string> findResults;
        private bool hasFindingBeenPerformed;

        public static SecuredEntitiesFinder getInstance()
        {
            if(_securedEntitiesFinder == null)
                _securedEntitiesFinder = new SecuredEntitiesFinder();
            return _securedEntitiesFinder;
        }

        private SecuredEntitiesFinder()
        {
            findResults = new List<string>();
            hasFindingBeenPerformed = false;
        }

        public List<string> GetSecuredEntitiesNames()
        {
            if(!hasFindingBeenPerformed)
                FindSecuredEntities();
            return findResults;
        }

        private void FindSecuredEntities()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            IEnumerable<Type> protectedEntities = GetTypesWithSecureAttribute(assembly);

            foreach (var type in protectedEntities)
            {
                var attribute =
                    type.GetCustomAttributes(typeof(SecureAttribute), false).FirstOrDefault() as SecureAttribute;
                if (attribute.RelationName.Equals(""))
                {
                    //Console.WriteLine(type.Name.ToLower());
                    findResults.Add(type.Name.ToLower());
                }
                else
                {
                    //Console.WriteLine(attribute.RelationName);
                    findResults.Add(attribute.RelationName);
                } 
            }
            
            hasFindingBeenPerformed = true;
        }
        
        private static IEnumerable<Type> GetTypesWithSecureAttribute(Assembly assembly) {
            foreach(Type type in assembly.GetTypes()) {
                if (type.GetCustomAttributes(typeof(SecureAttribute), true).Length > 0) {
                    yield return type;
                }
            }
        }
    }
}