using System;
using NHibernate.Hql.Ast;
using NHibernate.Type;

namespace ConsoleApplication1.Security.Secure
{
    [AttributeUsage(AttributeTargets.Class, Inherited = false)]
    public class SecureAttribute : Attribute
    {
        private string _relationName;
        public SecureAttribute()
        {
            _relationName = "";
        }

        public virtual string RelationName
        {
            get { return _relationName; }
            set { _relationName = value; }
        }
    }
}