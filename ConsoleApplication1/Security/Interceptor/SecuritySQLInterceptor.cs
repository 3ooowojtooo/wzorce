using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using ConsoleApplication1.ACL;
using ConsoleApplication1.Security.Context;
using ConsoleApplication1.Security.Secure;
using NHibernate;
using NHibernate.SqlCommand;

namespace ConsoleApplication1.Security
{
    public class SecuritySQLInterceptor : EmptyInterceptor, IInterceptor
    {
        SqlString IInterceptor.OnPrepareStatement(SqlString sqlString)
        {
            if (!shouldQueryBeParsed(sqlString))
            {
                Console.WriteLine("The query will not be parsed.(INSERT or sequence query)");
                return sqlString;
            }

            string tableName = extractTableName(sqlString).ToLower();

            if (!isTableProtected(tableName))
            {
                Console.WriteLine("The query will not be parsed.(Framework Query)");
                return sqlString;
            }

            AclSystem aclSystem = new AclSystemImpl();
            var parseResults = aclSystem.getParseResults(tableName,
                SimpleSecurityContext.getSecurityContext().getUserRole().role_name);

            Console.WriteLine("Query will be parsed.");
            Console.WriteLine("No. of allowed Ids: " + parseResults.allowedIds().Count);

            if (sqlString.StartsWithCaseInsensitive("SELECT"))
            {
                return parseSelectSqlString(sqlString, parseResults);
            }

            return parseUpdateDeleteSqlString(sqlString, parseResults);
        }

        private bool shouldQueryBeParsed(SqlString sqlString)
        {
            return !sqlString.StartsWithCaseInsensitive("INSERT") &&
                   !sqlString.StartsWithCaseInsensitive("SELECT nextval");
        }

        private bool isTableProtected(string tableName)
        {
            SecuredEntitiesFinder securedEntitiesFinder = SecuredEntitiesFinder.getInstance();
            List<string> securedEntitiesNames = securedEntitiesFinder.GetSecuredEntitiesNames();
            return securedEntitiesNames.Contains(tableName);
//            return !tableName.Equals("ROLE", StringComparison.InvariantCultureIgnoreCase) &&
//                   !tableName.Equals("ACL", StringComparison.InvariantCultureIgnoreCase);
        }

        private string extractTableName(SqlString sqlString)
        {
            if (sqlString.StartsWithCaseInsensitive("UPDATE"))
            {
//                UPDATE statement
                var tempSqlString = sqlString.SubstringStartingWithLast("UPDATE");

                var regex = new Regex("UPDATE\\s(\\w+).*");
                var match = regex.Match(tempSqlString.ToString());

                return match.Groups[1].ToString();
            }
            else
            {
//                SELECT or DELETE statements
                var tempSqlString = sqlString.SubstringStartingWithLast("FROM");

                var regex = new Regex("FROM\\s(\\w+)\\s.*");
                var match = regex.Match(tempSqlString.ToString());

                return match.Groups[1].Value;
            }
        }

        private SqlString parseSelectSqlString(SqlString sqlString, ParseResults parseResults)
        {
            var conditionReplacementString = prepareConditionReplacementString(parseResults);
            if (sqlString.LastIndexOfCaseInsensitive("WHERE") == -1)
            {
//                WHERE IS NOT PRESENT
                if (sqlString.LastIndexOfCaseInsensitive("ORDER BY") == -1)
                {
                    return sqlString.Append(" WHERE " + conditionReplacementString);
                }

                return sqlString.Replace("ORDER BY",
                    "WHERE " + conditionReplacementString + " ORDER BY");
            }

//            WHERE IS PRESENT
            return sqlString.Replace("WHERE", "WHERE " + conditionReplacementString + " AND");
        }


        private SqlString parseUpdateDeleteSqlString(SqlString sqlString, ParseResults parseResults)
        {
//            UPDATE statement always has to 1 as a number of rows updated.
            if (parseResults.allowedIds().Count == 0)
            {
//                DUMMY SQL
                return new SqlString("UPDATE acl SET priority=priority WHERE id = -1");
            }

            return sqlString.Replace("WHERE", "WHERE " + prepareConditionReplacementString(parseResults) + " AND");
        }

        private string prepareConditionReplacementString(ParseResults parseResults)
        {
            if (parseResults.allowedIds().Count == 0)
            {
                return "FALSE";
            }
            else
            {
                return "Id IN (" + string.Join(",", parseResults.allowedIds()) + ")";
            }
        }
    }
}