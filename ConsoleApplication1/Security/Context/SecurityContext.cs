using ConsoleApplication1.Domain;

namespace ConsoleApplication1.Security.Context
{
    public interface SecurityContext
    {
        Role getUserRole();
    }
}