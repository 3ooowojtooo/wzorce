using ConsoleApplication1.Domain;

namespace ConsoleApplication1.Security.Context
{
    public class SimpleSecurityContext : SecurityContext
    {
        private static SimpleSecurityContext _simpleSecurityContext;
        private readonly Role currentUserRole;

        private SimpleSecurityContext(Role currentUserRole)
        {
            this.currentUserRole = currentUserRole;
        }

        public static SecurityContext getSecurityContext()
        {
            if (_simpleSecurityContext == null)
            {
                var role = new Role {role_id = 102, role_name = "BASIC_USER"};
                _simpleSecurityContext = new SimpleSecurityContext(role);
            }

            return _simpleSecurityContext;
        }

        public Role getUserRole()
        {
            return currentUserRole;
        }
    }
}