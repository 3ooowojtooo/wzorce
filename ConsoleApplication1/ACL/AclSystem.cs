using ConsoleApplication1.Repositories;

namespace ConsoleApplication1.ACL
{
    public abstract class AclSystem
    {
        public ParseResults getParseResults(string tableName, string role)
        {
            IAclRepository aclRepository = new AclRepository();
            IAclParser parser = createParser();
            return parser.parse(aclRepository.getAllAclsForRoleByTable(tableName, role));
        }

        protected abstract IAclParser createParser();
    }
}