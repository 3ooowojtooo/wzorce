using System.Collections.Generic;

namespace ConsoleApplication1.ACL
{
    class ParseResultsImpl : ParseResults
    {
        private readonly List<int> _allowed;

        public ParseResultsImpl(List<int> allowedIds)
        {
            this._allowed = allowedIds;
        }

        public List<int> allowedIds()
        {
            return _allowed;
        }
    }
}