using System.Collections.Generic;

namespace ConsoleApplication1.ACL
{
    public interface ParseResults
    {
        List<int> allowedIds();
    }
}