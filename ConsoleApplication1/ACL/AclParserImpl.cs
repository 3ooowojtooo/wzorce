using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using ConsoleApplication1.Domain;

namespace ConsoleApplication1.ACL
{
    class AclParserImpl : IAclParser
    {
        private List<int> allowedIds;
        public ParseResults parse(List<Acl> acls)
        {
            allowedIds = new List<int>();
            foreach (var acl in acls)
            {
                processAcl(acl);
            }

            return generateParseResults();
        }

        private void processAcl(Acl acl)
        {
            List<int> currentAllowedIds = parseAllowedIdsString(acl.Allowed_Ids);
            foreach (var id in currentAllowedIds)
            {
                if(!this.allowedIds.Contains(id))
                    this.allowedIds.Add(id);
            }
        }

        private List<int> parseAllowedIdsString(string allowedIdsStr)
        {
            List<string> idsStr = allowedIdsStr.Split(',').ToList();
            List<int> result = new List<int>();
            foreach (var id in idsStr)
            {
                result.Add(int.Parse(id));
            }

            return result;
        }

        private ParseResults generateParseResults()
        {
            return new ParseResultsImpl(allowedIds);
        }
   }
}