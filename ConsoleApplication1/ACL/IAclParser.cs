using System;
using System.Collections;
using System.Collections.Generic;
using ConsoleApplication1.Domain;

namespace ConsoleApplication1.ACL
{
    public interface IAclParser
    {
        ParseResults parse(List<Acl> acls);
    }
}