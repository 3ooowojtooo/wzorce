using System.Collections.Generic;
using ConsoleApplication1.Domain;

namespace ConsoleApplication1.ACL
{
    public interface IRoleRepository
    {
        Role GetById(int roleId);

        Role GetByRoleName(string roleName);

        List<Role> GetAllRolesFromInheritanceTree(string roleName);
    }
}