using System;
using System.Collections.Generic;
using ConsoleApplication1.ACL;
using ConsoleApplication1.Domain;
using NHibernate;
using NHibernate.Criterion;

namespace ConsoleApplication1.Repositories
{
    public class AclRepository : IAclRepository
    {
        public List<Acl> getAllAclsForRoleByTable(string tableName, string role)
        {
            IRoleRepository roleRepository = new RoleRepository();
            List<Role> rolesFamilly = roleRepository.GetAllRolesFromInheritanceTree(role);
            List<Acl> result = new List<Acl>();
            foreach (var currentRole in rolesFamilly)
            {
                string currentRoleName = currentRole.role_name;
                ICollection<Acl> currentAcls = getByTableAndRoleSorted(tableName, currentRoleName);
                foreach (var currentAcl in currentAcls)
                {
                    result.Add(currentAcl);
                }
            }

            return result;
        }

        private ICollection<Acl> getByTableAndRoleSorted(string tableName, string roleName)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                var result = session.CreateCriteria(typeof(Acl)).Add(Restrictions.Eq("Table_Name", tableName))
                    .Add(Restrictions.Eq("Role_Name", roleName))
                    .AddOrder(Order.Asc("Priority")).List<Acl>();
                return result;
            }
        }
    }
}