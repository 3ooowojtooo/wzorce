using System.Collections.Generic;
using ConsoleApplication1.ACL;
using ConsoleApplication1.Domain;
using NHibernate;
using NHibernate.Criterion;

namespace ConsoleApplication1.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        public Role GetById(int roleId)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                return session.Get<Role>(roleId);
            }
        }

        public Role GetByRoleName(string roleName)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                Role role = session.CreateCriteria(typeof(Role)).Add(Restrictions.Eq("role_name", roleName))
                    .UniqueResult<Role>();
                return role;
            }
        }

        public List<Role> GetAllRolesFromInheritanceTree(string roleName)
        {
            List<Role> result = new List<Role>();
            Role firstRole = GetByRoleName(roleName);
            result.Add(firstRole);
            int nextRoleId = firstRole.parent_id;
            while (nextRoleId != 0)
            {
                Role current = GetById(nextRoleId);
                result.Add(current);
                nextRoleId = current.parent_id;
            }

            result.Reverse();
            return result;
        }
    }
}