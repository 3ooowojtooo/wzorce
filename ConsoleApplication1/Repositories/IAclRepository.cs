using System;
using System.Collections.Generic;
using ConsoleApplication1.ACL;
using ConsoleApplication1.Domain;

namespace ConsoleApplication1.Repositories
{
    public interface IAclRepository
    {
        List<Acl> getAllAclsForRoleByTable(string tableName, string role);
    }
}