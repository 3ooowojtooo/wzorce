using System.Collections.Generic;

namespace ConsoleApplication1.Domain
{
    public interface IProductRepository
    {
        int Add(Product product);

        void Update(Product product);

        void Remove(Product product);

        Product GetById(int productId);

        Product GetByName(string name);

        ICollection<Product> GetByCategory(string category);

        ICollection<Product> GetAll();
        
        ICollection<Product> GetAllOrderById();
    }
}