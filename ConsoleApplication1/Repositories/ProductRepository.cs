using System.Collections.Generic;
using ConsoleApplication1.Domain;
using NHibernate;
using NHibernate.Criterion;

namespace ConsoleApplication1.Repositories
{
    public class ProductRepository : IProductRepository
    {
        public int Add(Product product)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                var productId = (int) session.Save(product);
                transaction.Commit();
                return productId;
            }
        }

        public void Update(Product product)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Update(product);
                transaction.Commit();
            }
        }

        public void Remove(Product product)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            using (ITransaction transaction = session.BeginTransaction())
            {
                session.Delete(product);
                transaction.Commit();
            }
        }

        public Product GetById(int productId)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                return session.Get<Product>(productId);
            }
        }

        public Product GetByName(string name)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                Product product = session.CreateCriteria(typeof(Product)).Add(Restrictions.Eq("Name", name))
                    .UniqueResult<Product>();
                return product;
            }
        }

        public ICollection<Product> GetByCategory(string category)
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                var products = session.CreateCriteria(typeof(Product)).Add(Restrictions.Eq("Category", category))
                    .List<Product>();
                return products;
            }
        }

        public ICollection<Product> GetAll()
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                var product = session.CreateCriteria(typeof(Product))
                    .List<Product>();
                return product;
            }
        }

        public ICollection<Product> GetAllOrderById()
        {
            using (ISession session = NHibernateHelper.OpenSession())
            {
                var product = session.CreateCriteria(typeof(Product)).AddOrder(Order.Asc("Id"))
                    .List<Product>();
                return product;
            }
        }
    }
}