﻿using System;
using System.Collections.Generic;
using ConsoleApplication1.Domain;
using ConsoleApplication1.Repositories;
using ConsoleApplication1.Security.Secure;

namespace ConsoleApplication1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
//            AclSystem aclSystem = new AclSystemImpl();
//            ParseResults result = aclSystem.getParseResults("product", "USER21");
//            Console.WriteLine("Product IDs that a user USER21 is whitelisted for:");
//            foreach (var a in result.allowedIds())
//            {
//                Console.WriteLine(a);
//            }

            IProductRepository productRepository = new ProductRepository();

            Product newProduct = new Product();
            newProduct.Name = "test";
            newProduct.Category = "test";
            newProduct.Discontinued = true;

//            INSERT
            Console.WriteLine("INSERT");
            var newProductId = productRepository.Add(newProduct);
            Console.WriteLine(newProduct);

            Console.WriteLine("--------------------------------");

//            SELECT
            Console.WriteLine("SELECT a product with ID: " + newProductId);
            var deniedProduct = productRepository.GetById(newProductId);
            if (deniedProduct == null)
            {
                Console.WriteLine("The user doesn't have the privilege to access a product with ID " + newProductId);
            }
            else
            {
                Console.WriteLine(deniedProduct);
            }

            Console.WriteLine("--------------------------------");

//            SELECT
            var existingProductId = 101;
            Console.WriteLine("SELECT a product with ID: " + existingProductId);
            var existingProduct = productRepository.GetById(existingProductId);
            if (existingProduct == null)
            {
                Console.WriteLine("The user doesn't have the privilege to access a product with ID: " +
                                  existingProductId);
            }
            else
            {
                Console.WriteLine(existingProduct);
            }

            Console.WriteLine("--------------------------------");

//            UPDATE
            newProduct.Name = "superTest";
            Console.WriteLine("UPDATE a product with ID: " + existingProductId);
            productRepository.Update(newProduct);
            Console.WriteLine(newProduct);
            Console.WriteLine("--------------------------------");

//            DELETE
            Console.WriteLine("DELETE a product with ID: " + newProduct.Id);
            productRepository.Remove(newProduct);
            Console.WriteLine("--------------------------------");

//            SELECT ALL
            Console.WriteLine("SELECT ALL");
            ICollection<Product> products = productRepository.GetAll();
            Console.WriteLine("Found: " + products.Count + " products");
            Console.WriteLine("--------------------------------");

//            SELECT ALL
            Console.WriteLine("SELECT ALL");
            ICollection<Product> productsOrdered = productRepository.GetAllOrderById();
            Console.WriteLine("Found: " + productsOrdered.Count + " products");
        }
    }
}