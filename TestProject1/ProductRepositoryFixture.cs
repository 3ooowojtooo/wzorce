using ConsoleApplication1.Domain;
using ConsoleApplication1.Repositories;
using NHibernate;
using NHibernate.Cfg;
using NUnit.Framework;

namespace TestProject1
{
    [TestFixture]
    public class ProductRepositoryFixture
    {
        private ISessionFactory _sessionFactory;
        private Configuration _configuration;


        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            _configuration = new Configuration();
            _configuration.Configure();
            _configuration.AddAssembly(typeof(Product).Assembly);
            _sessionFactory = _configuration.BuildSessionFactory();
        }

        [SetUp]
        public void SetupContext()
        {
//            new SchemaExport(_configuration).Execute(false, true, false, false);
        }

        [Test]
        public void CanAddNewProduct()
        {
            var product = new Product {Name = "Apple", Category = "Fruits"};
            IProductRepository repository = new ProductRepository();
            repository.Add(product);
        }
    }
}