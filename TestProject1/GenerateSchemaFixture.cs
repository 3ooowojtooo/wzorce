using ConsoleApplication1.Domain;
using NHibernate.Cfg;
using NUnit.Framework;

namespace TestProject1
{
    [TestFixture]
    public class GenerateSchema_Fixture
    {
        [Test]
        public void canRunNHibernate()

        {
            var cfg = new Configuration();
            cfg.Configure();
            cfg.AddAssembly(typeof(Product).Assembly);
        }
    }
}